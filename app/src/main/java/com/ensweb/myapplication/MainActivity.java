package com.ensweb.myapplication;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //associer layout activitymain.xml
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_add_annonce, R.id.nav_my_list, R.id.nav_preference)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        SharedPreferences myPreference = Objects.requireNonNull(getApplicationContext()).getSharedPreferences("myPreference", 0);
        //récupérer layout nav_header_main avec une méthode diff vu que dans notre setcontent on a mis l'activity main
        View navHeader = LayoutInflater.from(this).inflate(R.layout.nav_header_main, null);
        //récupérer le pseudo et l'email de la préference
        ((TextView) navHeader.findViewById(R.id.pseudo_header)).setText(myPreference.getString("pseudo", null));
        ((TextView) navHeader.findViewById(R.id.email_header)).setText(myPreference.getString("email", null));
        // ajouter le navheader à notre navigation view avec la méthode prédéfinie addheaderview
        navigationView.addHeaderView(navHeader);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}
