package com.ensweb.myapplication.ui.annonce;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.ensweb.myapplication.R;
import com.ensweb.myapplication.ui.annonce.models.Annonce;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddAnnonceFragment extends Fragment {

    private SharedPreferences myPreference;
    private String pseudo;
    private String email;
    private String tel;
    private String titre;
    private String description;
    private String prix;
    private String ville;
    private String cp;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_add_annonce, container, false);
        // pseudoText = root.findViewById(R.id.pseudo);
        this.myPreference = Objects.requireNonNull(getContext()).getSharedPreferences("myPreference", 0); // 0 - for private mode

        pseudo = myPreference.getString("pseudo", null);
        email = myPreference.getString("email", null);
        tel = myPreference.getString("tel", null);

        Button button = root.findViewById(R.id.saveAnnonce);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titre = String.valueOf(((EditText) root.findViewById(R.id.titre)).getText());
                description = String.valueOf(((EditText) root.findViewById(R.id.description)).getText());
                prix = String.valueOf(((EditText) root.findViewById(R.id.prix)).getText());
                ville = String.valueOf(((EditText) root.findViewById(R.id.ville)).getText());
                cp = String.valueOf(((EditText) root.findViewById(R.id.cp)).getText());
                if(pseudo == null || email == null || tel == null){
                    Toast.makeText(getContext(), "Vous devez saisir vos préference", Toast.LENGTH_SHORT).show();
                }else if(titre.equals("")){
                    Toast.makeText(getContext(), "Vous devez saisir le titre de l'annonce", Toast.LENGTH_SHORT).show();
                }else{
                    OkHttpAddHandler okHttpHandler = new OkHttpAddHandler();
                    okHttpHandler.execute("https://ensweb.users.info.unicaen.fr/android-api/");
                }
            }
        });
        return root;
    }

    public class OkHttpAddHandler extends AsyncTask<String, Void, Boolean> {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected Boolean doInBackground(String... strings) {

            String numEtudiant = "21913538";
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("apikey", numEtudiant)
                    .addFormDataPart("method", "save")
                    .addFormDataPart("titre", titre)
                    .addFormDataPart("description", description)
                    .addFormDataPart("prix", prix)
                    .addFormDataPart("pseudo", pseudo)
                    .addFormDataPart("emailContact", email)
                    .addFormDataPart("telContact", tel)
                    .addFormDataPart("ville", ville)
                    .addFormDataPart("cp", cp)
                    .build();

            Request request = new Request.Builder().url(String.valueOf(strings[0]))
                    .post(requestBody)
                    .build();
            try {
                Response response = client.newCall(request).execute();
                JSONObject jsonObject = new JSONObject(Objects.requireNonNull(response.body()).string());
                return jsonObject.getBoolean("success");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            Toast.makeText(getContext(), success ? "Ajouter avec succés" : "L'ajout a échouée", Toast.LENGTH_SHORT).show();
        }
    }
}