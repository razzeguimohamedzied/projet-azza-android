package com.ensweb.myapplication.ui.annonce;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ensweb.myapplication.R;
import com.ensweb.myapplication.ui.annonce.models.Annonce;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AnnonceFragment extends Fragment {

    private RecyclerView recyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_annonce, container, false);
        // final TextView textView = root.findViewById(R.id.text_home);

        Bundle bundle = getArguments();
        boolean isMyList = Objects.requireNonNull(bundle).getBoolean("isMyList");
        String pseudo = null;

        if (isMyList) {
            SharedPreferences myPreference = Objects.requireNonNull(getContext()).getSharedPreferences("myPreference", 0);
            pseudo = myPreference.getString("pseudo", null);
        }

        OkHttpListHandler okHttpHandler = new OkHttpListHandler();
        String numEtudiant = "21913538";
        String url = "https://ensweb.users.info.unicaen.fr/android-api/?apikey=" + numEtudiant + "&method=";
        url += isMyList ? "listByPseudo&pseudo=" + pseudo : "listAll";
        okHttpHandler.execute(url);

        this.recyclerView = root.findViewById(R.id.recyclerView);

        //définit l'agencement des cellules, ici de façon verticale, comme une ListView
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return root;
    }


    public class OkHttpListHandler extends AsyncTask<String, Void, List<Annonce>> {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected List<Annonce> doInBackground(String... strings) {

            Request.Builder builder = new Request.Builder();
            builder.url(String.valueOf(strings[0]));
            Request request = builder.build();

            try {
                Response response = client.newCall(request).execute();
                JSONObject jsonObject = new JSONObject(Objects.requireNonNull(response.body()).string());
                JSONArray array = jsonObject.getJSONArray("response");
                List<Annonce> annonces = new ArrayList<>();
                for (int i = 0; i < array.length(); i++) {
                    annonces.add(new Annonce(array.getJSONObject(i)));
                }
                return annonces;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Annonce> responseAnnonces) {
            super.onPostExecute(responseAnnonces);
            handleAnnonces(responseAnnonces);
        }
    }

    public void handleAnnonces(List<Annonce> annonces) {
        AnnonceAdapter adapter = new AnnonceAdapter(annonces);
        // dire au recyclerView d'utiliser l'adapter créé
        this.recyclerView.setAdapter(adapter);
    }


    public class AnnonceAdapter extends RecyclerView.Adapter<AnnonceViewHolder> {

        List<Annonce> list;

        // donner la liste à l'adapteur
        AnnonceAdapter(List<Annonce> list) {
            this.list = list;
        }

        // créer les conteneurs de vue
        // et leur dire quel layout utiliser pour un item
        @Override
        public AnnonceViewHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_annonce_item, viewGroup, false);
            return new AnnonceViewHolder(view);
        }

        // méthode appelée lorsque un conteneur de vue (pour un item) reçoit l'objet qui servira à remplir l'item
        // dire alors au conteneur de vue de faire ce "bind"
        @Override
        public void onBindViewHolder(AnnonceViewHolder annonceViewHolder, int position) {
            Annonce annonce = list.get(position);
            annonceViewHolder.bind(annonce);
        }

        // méthode obligatoire car déclarée abstraite dans la class mère
        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }

    }

    // contenueur de vues de Personnes
    class AnnonceViewHolder extends RecyclerView.ViewHolder {

        private TextView vueTitre;
        private TextView vueDesc;
        private TextView vueTel;
        private TextView vuePrix;
        private ConstraintLayout container;

        // itemView est la vue correspondante à 1 item
        AnnonceViewHolder(View itemView) {
            super(itemView);
            //obtenir les éléments de la vue d'un item
            vueTitre = itemView.findViewById(R.id.itemTitre);
            vueDesc = itemView.findViewById(R.id.itemDesc);
            vueTel = itemView.findViewById(R.id.itemTel);
            vuePrix = itemView.findViewById(R.id.itemPrice);
            container = itemView.findViewById(R.id.ietmContainer);
        }

        // sert à remplir les champs d'un item avec les données de l'objet fourni
        void bind(final Annonce annonce) {
            container.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent i = new Intent(getContext(), DetailsAnnonceActivity.class);
                    i.putExtra("id", annonce.getId());
                    startActivity(i);
                }
            });
            vueTitre.setText(annonce.getTitre());
            int tailleDescription = 150;
            String description;
            if (annonce.getDescription().length() > tailleDescription) {
                description = annonce.getDescription().substring(0, tailleDescription) + "...";
            } else {
                description = annonce.getDescription();
            }
            vueDesc.setText(description);

            String tel = "Tel:" + annonce.getTelContact();
            vueTel.setText(tel);

            String price = annonce.getPrix() + "€";
            vuePrix.setText(price);
        }
    }
}