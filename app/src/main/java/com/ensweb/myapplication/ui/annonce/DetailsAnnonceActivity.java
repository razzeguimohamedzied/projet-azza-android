package com.ensweb.myapplication.ui.annonce;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import com.ensweb.myapplication.R;
import com.ensweb.myapplication.databinding.ActivityDetailsAnnonceBinding;
import com.ensweb.myapplication.ui.annonce.models.Annonce;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

public class DetailsAnnonceActivity extends AppCompatActivity {

    Context context;
    ActivityDetailsAnnonceBinding binding;
    CarouselView carouselView;
    List<Bitmap> bitmaps;
    int pasImageDrawable = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        bitmaps = new ArrayList<>();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_annonce);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details_annonce);
        context = getApplicationContext();
        carouselView = findViewById(R.id.carouselView);
        carouselView.setPageCount(0);

        // toolbar
        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        String id;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                id = null;
            } else {
                id = extras.getString("id");
            }
        } else {
            id = (String) savedInstanceState.getSerializable("id");
        }

        OkHttpHandler okHttpHandler = new OkHttpHandler();
        String numEtudiant = "21913538";
        String url = "https://ensweb.users.info.unicaen.fr/android-api/?apikey=" + numEtudiant + "&method=details&id="+id;
        okHttpHandler.execute(url);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    static class MyAsync extends AsyncTask<List<String>, Void, List<Bitmap>> {

        @Override
        protected List<Bitmap> doInBackground(List<String>... passing) {

            try {
                List<String> passed = passing[0]; //get passed arraylist
                List<Bitmap> bitmaps = new ArrayList<>();
                for (String uri : passed) {
                    URL url = new URL(uri);
                    bitmaps.add(BitmapFactory.decodeStream(url.openConnection().getInputStream()));
                }
                return bitmaps;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

        }
    }


    public class OkHttpHandler extends AsyncTask<String, Void, Annonce> {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected Annonce doInBackground(String... strings) {

            Request.Builder builder = new Request.Builder();
            builder.url(String.valueOf(strings[0]));
            Request request = builder.build();

            try {
                Response response = client.newCall(request).execute();
                JSONObject jsonObject = new JSONObject(Objects.requireNonNull(response.body()).string());
                return new Annonce(jsonObject.getJSONObject("response"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Annonce responseAnnonce) {
            super.onPostExecute(responseAnnonce);
            handleAnnonce(responseAnnonce);
            binding.setAnnonce(responseAnnonce);
        }
    }

    public void handleAnnonce(final Annonce annonce) {
        if (annonce.getImages() != null && annonce.getImages().size() > 0) {
            MyAsync obj = new MyAsync() {

                @Override
                protected void onPostExecute(List<Bitmap> bmps) {
                    super.onPostExecute(bmps);
                    bitmaps = bmps;
                    carouselView.setImageListener(imageListener);
                    carouselView.setPageCount(bmps != null ? bmps.size() : 0);

                    if(bmps == null){
                        pasImageDrawable = R.drawable.pas_image;
                        carouselView.setImageListener(pasImageListener);
                        carouselView.setPageCount(1);
                    }
                }
            };
            for (int i = 0; i < annonce.getImages().size(); i++) {
                annonce.getImages().set(i, annonce.getImages().get(i)
                        .replace("https", "http").replace("http", "https"));
            }

            obj.execute(annonce.getImages());
        } else {
            pasImageDrawable = R.drawable.pas_image;
            carouselView.setImageListener(pasImageListener);
            carouselView.setPageCount(1);
        }

        Button contactEmail = findViewById(R.id.contactEmail);
        contactEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{annonce.getEmailContact()});
                email.putExtra(Intent.EXTRA_SUBJECT, "Annonce " + annonce.getTitre());
                email.putExtra(Intent.EXTRA_TEXT, "Je Vous contact par rapport l'annonce " + annonce.getTitre());
                email.setType("message/rfc822");

                startActivity(Intent.createChooser(email, "Choose an Email client :"));
            }
        });

        Button contactTel = findViewById(R.id.contactTel);
        contactTel.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + annonce.getTelContact()));

                if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(intent);
            }
        });
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageBitmap(bitmaps.get(position));
        }
    };

    ImageListener pasImageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(pasImageDrawable);
        }
    };
}