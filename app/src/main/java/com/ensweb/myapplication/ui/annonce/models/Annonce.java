package com.ensweb.myapplication.ui.annonce.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Annonce {
    private String id;
    private String titre;
    private String description;
    private int prix;
    private String pseudo;
    private String emailContact;
    private String telContact;
    private String ville;
    private String cp;
    private List<String> images;
    private Long date;

    public Annonce(){

    }

    public Annonce(JSONObject jsonObject) throws JSONException {
        this.id = jsonObject.getString("id");
        this.titre = jsonObject.getString("titre");
        this.description = jsonObject.getString("description");
        this.prix = jsonObject.getInt("prix");
        this.pseudo = jsonObject.getString("pseudo");
        this.emailContact = jsonObject.getString("emailContact");
        this.telContact = jsonObject.getString("telContact");
        this.ville = jsonObject.getString("ville");
        this.cp = jsonObject.getString("cp");

        JSONArray array = jsonObject.getJSONArray("images");
        this.images = new ArrayList<>();
        for (int i = 0; i < array.length(); i++){
            this.images.add(array.getString(i));
        }

        this.date = jsonObject.getLong("date");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getEmailContact() {
        return emailContact;
    }

    public void setEmailContact(String emailContact) {
        this.emailContact = emailContact;
    }

    public String getTelContact() {
        return telContact;
    }

    public void setTelContact(String telContact) {
        this.telContact = telContact;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
}
