package com.ensweb.myapplication.ui.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.ensweb.myapplication.R;

import java.util.Objects;

public class PreferenceFragment extends Fragment {

    private SharedPreferences myPreference;
    private EditText pseudoText;
    private EditText emailText;
    private EditText telText;
    private Context context;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_preference, container, false);
        context = getContext();
        pseudoText = root.findViewById(R.id.pseudo);
        emailText = root.findViewById(R.id.email);
        telText = root.findViewById(R.id.tel);
        Button button = root.findViewById(R.id.savePreference);

        this.myPreference = Objects.requireNonNull(getContext()).getSharedPreferences("myPreference", 0); // 0 - for private mode

        pseudoText.setText(myPreference.getString("pseudo", null));
        emailText.setText(myPreference.getString("email", null));
        telText.setText(myPreference.getString("tel", null));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = myPreference.edit();
                editor.putString("pseudo", pseudoText.getText().toString());
                editor.putString("email", emailText.getText().toString());
                editor.putString("tel", telText.getText().toString());
                editor.apply();
                Toast.makeText(context, "Nous avons conservé vos coordonnées", Toast.LENGTH_SHORT).show();
            }
        });
        return root;
    }
}